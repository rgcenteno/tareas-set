/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tareassets;

import java.util.Set;
import java.util.HashSet;
/**
 *
 * @author rgcenteno
 */
public class TareasSets {    
    private static java.util.Scanner teclado;
    
    public static void main(String[] args) {        
        
        teclado = new java.util.Scanner(System.in);
        
        String opcion = "";
        do{
            System.out.println("**************************************");
            System.out.println("* 1. Conjunto enteros                *");
            System.out.println("* 2. Comprobar palabras              *");
            System.out.println("* 3. Palabras formadas mismas letras *");
            System.out.println("* 4. Contiene todas las letras       *");
            System.out.println("* 5. Otro de primos                  *");
            System.out.println("*                                    *");
            System.out.println("* 0. Salir                           *");
            System.out.println("**************************************"); 
            opcion = teclado.nextLine();
            String nombre;
            switch(opcion){
                case "1":
                    programa1();
                    break;
                case "2":
                    programa2();
                    break;
                case "3":
                    programa3();
                    break;
                case "4":
                    programa4();
                    break;
                case "5":
                    programa5();
                    break;
                case "0":                    
                    break;
                default:
                    System.out.println("Opción inválida.");
                    break;
            }
        }while(!opcion.equals("0"));
    }    
    
    private static void programa1(){
        Set<Integer> conjunto = new HashSet<>();
        
        int numero = -2;
        do{
            System.out.println("Inserte un nuevo número");            
            if(teclado.hasNextInt()){
                numero = teclado.nextInt();
                if(numero != -1){
                    if(!conjunto.add(numero)){
                        System.out.println("No se ha podido insertar el número: " + numero +". El elemento ya existe. ");
                    }
                }
            }
            else{
                System.out.println("Debe insertar un número entero");
            }
            teclado.nextLine();
        }
        while(numero != -1);
    }
    
    private static void programa2(){
        System.out.println("Inserte la frase a insertar:");
        String frase = teclado.nextLine().trim();
        frase = java.util.regex.Pattern.compile("[^a-zA-Z0-9 ]").matcher(frase).replaceAll("");
        //frase = frase.replaceAll("[^a-zA-Z0-9 ]", "");
        String[] palabras = frase.split(" +");
        Set<String> conjuntoPalabras = new HashSet<>();
        for(String palabra : palabras){
            conjuntoPalabras.add(palabra);
        }
        String busqueda;
        do{
            System.out.println("Inserte la palabra que quiere comprobar si existe. Escriba fin para salir.");
            busqueda = teclado.nextLine();
            if(!busqueda.equalsIgnoreCase("fin")){
                if(conjuntoPalabras.contains(busqueda)){
                    System.out.println("La palabra SÍ existe en el conjunto");
                }
                else{
                    System.out.println("La palabra NO existe en el conjunto");
                }
            }
        }
        while(!busqueda.equalsIgnoreCase("fin"));
    }
    
    private static void programa3(){
        System.out.println("Inserte la primera palabra");
        String primera = teclado.nextLine().replaceAll("[^a-zA-Z]", "");
        System.out.println("Inserte la segunda palabra");
        String segunda = teclado.nextLine().replaceAll("[^a-zA-Z]", "");
        
        Set<Character> set1 = new HashSet<>();
        Set<Character> set2 = new HashSet<>();
        
        char[] characters1 = primera.toCharArray();
        char[] characters2 = segunda.toCharArray();
        
        for(char c : characters1){
            set1.add(c);
        }
        
        for(char c : characters2){
            set2.add(c);
        }
        
        System.out.printf("Ambas palabras %s están formadas por las mismas letras.\n", (set1.equals(set2) ? "SÍ" : "NO"));
    }
    
    private static void programa4(){
        System.out.println("Inserte los caracteres que debe contener la frase:");
        String caracteresAContener = teclado.nextLine();
        System.out.println("Inserte la frase a comprobar:");
        String frase = teclado.nextLine();
        Set<Character> setCaracteresAContener = new HashSet<>();
        for(int i = 0; i < caracteresAContener.length(); i++){
            setCaracteresAContener.add(caracteresAContener.charAt(i));
        }
        
        Set<Character> setFrase = new HashSet<>();
        for (int i = 0; i < frase.length(); i++) {
            setFrase.add(frase.charAt(i));            
        }
        
        System.out.printf("La frase %s contiene todos los caracteres\n", setFrase.containsAll(setCaracteresAContener) ? "SÍ" : "NO");
    }
    
    private static void programa5(){
        int n = -1;
        do{
            System.out.println("Inserte el número máximo a comprobar");
            if(teclado.hasNextInt()){
                n = teclado.nextInt();
                if(n <= 1){
                    System.out.println("Inserte un número mayor que 1");
                }                
            }
            else{
                System.out.println("Inserte un número mayor que 1");
            }
            teclado.nextLine();
        }while(n <= 1);
        Set<Integer> noPrimos = new HashSet<>();
        for(int baseMultiplos = 2; baseMultiplos * baseMultiplos <= n; baseMultiplos++){
            if(!noPrimos.contains(baseMultiplos)){
                for(int j = baseMultiplos; baseMultiplos * j <= n; j++){
                    noPrimos.add(baseMultiplos * j);
                }
            }
        }
        
        Set<Integer> primos = new java.util.TreeSet<>();
        for(int i = 2; i <= n; i++){
            primos.add(i);
        }
        
        primos.removeAll(noPrimos);
        System.out.println(primos);
    }
    
}
